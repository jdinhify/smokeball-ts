import { httpGet } from './mock-http-interface'

type TSuccess = { 'Arnie Quote': string }
type TFailure = { FAILURE: string }
type TResult = TSuccess | TFailure

export const getArnieQuotes = async (urls: string[]): Promise<TResult[]> => {
  const responses = await Promise.all(urls.map(httpGet))

  return responses.map((response) => {
    const { message } = JSON.parse(response.body)

    return response.status === 200
      ? { 'Arnie Quote': message }
      : { FAILURE: message }
  })
}
